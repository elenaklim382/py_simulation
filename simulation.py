from numpy import *
from tqdm import tqdm

arra = []

smena_p_ex = 0
smena_p_bu = 0

smena_r_ex = 0
smena_r_bu = 0

time_r_ex = 0
time_r_bu = 0

out_ex = 500
out_bu = 300

in_ex = 500
in_bu = 300

out_s6 = 100
out_s3s5 = 160

total = 0

out_all = 50

math_ex_s6 = 1
math_bu_s6 = 2
math_ex_s3s6 = 0.25
math_bu_s3s6 = 1.5


status_of_w = 0 #работник бездейств 0, работает 1

math_ex = 0
math_bu = 0
mnoj = 0

a = input("Выберите модель (слесарь 6 разряда -> 1 / слесарь 6 + 3 -> 2): ")
while a != "1" and a != "2":
    a = input("Ошибка, введите 1 либо 2: ")
if a == "1":
    math_ex = math_ex_s6
    math_bu = math_bu_s6
    mnoj = out_s6
elif a == "2":
    math_ex = math_ex_s3s6
    math_bu = math_bu_s3s6
    mnoj = out_s3s5

print("Загрузка...")
for j in tqdm(range(1000)):
    smena_p_ex = 0
    smena_p_bu = 0
    smena_r_ex = 0
    smena_r_bu = 0
    time_r_ex = 0
    time_r_bu = 0
    time_w_ex = 0
    time_w_bu = 0
    status_of_w = 0

    status_ex = 1
    status_bu = 1

    time_w_ex = round(float(random.exponential(4)) * 3600, 0)
    change_ex = time_w_ex

    time_w_bu = round(float(random.exponential(6)) * 3600, 0)
    change_bu = time_w_bu

    time_w_ex = round(float(random.exponential(4)) * 3600, 0)
    change_ex = time_w_ex
    time_w_bu = round(float(random.exponential(6)) * 3600, 0)
    change_bu = time_w_bu
    for i in range(16*3600):
        if (i == change_bu) or (i == change_ex):
            if i == 0:
                status_ex = 1
                status_bu = 1
            if change_ex < change_bu:
                if (status_of_w == 1) and (status_ex == 1):
                    status_ex = 0
                    smena_p_ex += (change_bu - change_ex)
                    change_ex += (change_bu - change_ex)
                if i == change_ex:
                    if (status_of_w == 0) and (status_ex != 2):
                        status_ex = 2
                        time_r_ex = round(float(random.exponential(math_ex))*3600, 0)
                        change_ex += time_r_ex
                        smena_r_ex += time_r_ex
                        status_of_w = 1
                    elif (status_ex == 2) and (status_of_w == 1):
                        status_ex = 1
                        status_of_w = 0
                        time_w_ex = round(float(random.exponential(4)) * 3600, 0)
                        change_ex += time_w_ex
            elif change_ex == change_bu:
                if status_ex == 2:
                    status_ex = 1
                    status_of_w = 0
                    time_w_ex = round(float(random.exponential(41)) * 3600, 0)
                    change_ex += time_w_ex
                    status_bu = 2
                    time_r_bu = round(float(random.exponential(math_bu)) * 3600, 0)
                    change_bu += time_r_bu
                    smena_r_bu += time_r_bu
                    status_of_w = 1
                elif status_bu == 2:
                    status_bu = 1
                    status_of_w = 0
                    time_w_bu = round(float(random.exponential(6)) * 3600, 0)
                    change_bu += time_w_bu
                    status_ex = 2
                    time_r_ex = round(float(random.exponential(math_ex)) * 3600, 0)
                    change_ex += time_r_ex
                    smena_r_ex += time_r_ex
                    status_of_w = 1
            else:
                if status_of_w == 1 and status_bu == 1:
                    status_bu = 0
                    smena_p_bu += (change_ex - change_bu)

                    change_bu += (change_ex - change_bu)
                if i == change_bu:
                    if status_of_w == 0 and status_bu != 2:
                        status_bu = 2
                        time_r_bu = round(float(random.exponential(math_bu))*3600,0)
                        change_bu+=time_r_bu
                        smena_r_bu+=time_r_bu
                        status_of_w = 1
                    elif status_bu == 2 and status_of_w == 1:
                        status_bu = 1
                        status_of_w = 0
                        time_w_bu = round(float(random.exponential(6)) * 3600,0)
                        change_bu += time_w_bu

    all_time_w_ex = round((16 * 3600 - (smena_p_ex + smena_r_ex)) / 3600, 1)
    all_time_w_bu = round((16 * 3600 - (smena_p_bu + smena_r_bu)) / 3600, 1)


    time_s_ex = round(smena_p_ex/3600, 1)
    time_s_bu = round(smena_p_bu/3600, 1)

    time_repair_all = round((smena_r_ex + smena_r_bu) / 3600, 1)
    time_repair_ex = round((smena_r_ex)/3600, 1)
    time_repair_bu = round((smena_r_bu) / 3600, 1)

    del_money_ex = time_s_ex*out_ex
    del_money_bu = time_s_bu*out_bu


    zp = time_repair_all*mnoj+time_repair_all*out_all

    plus_ex = all_time_w_ex*in_ex
    plus_bu = all_time_w_bu*in_bu
    # print("Время работы машин, ч: ",all_time_w_ex,all_time_w_bu)
    # print("Время работы слесарей, ч: ",time_repair_all)
    # print("  --------------------------------------")
    # print("Убытки от простоя экскаватора: ",del_money_ex)
    # print("Убытки от простоя бульдозера: ",del_money_bu)
    # print("Зарплата слесарей: ",zp)
    # print("Прибыль от работы экскаватора: ",plus_ex)
    # print("Прибыль от работы бульдозера: ",plus_bu)
    # print("  --------------------------------------")
    # print("Итого прибыль: ",plus_bu+plus_ex-zp-del_money_ex-del_money_bu)
    # print("\n")
    dop = [all_time_w_ex, all_time_w_bu, time_repair_ex, del_money_ex, del_money_bu, zp, plus_ex, plus_bu, time_s_ex, time_s_bu, time_repair_bu]

    total += (plus_bu+plus_ex-zp-del_money_ex-del_money_bu)

    arra.append(dop)

print("Всего прибыли за 1000 дней: ",total)
c = input("Выберите день для вывода(-1 - отмена, всего 1000): ")
while c != "-1":
    if (int(c) >= 0) and (int(c) < 10000):
        c = int(c)
        print("Время работы машин, ч: ", arra[c][0], arra[c][1])
        print("Время работы слесарей, ч: ", arra[c][2], arra[c][10])
        print("Время простоя машин, ч: ", arra[c][8], arra[c][9])
        print("  --------------------------------------")
        print("Убытки от простоя экскаватора: ", arra[c][3])
        print("Убытки от простоя бульдозера: ", arra[c][4])
        print("Зарплата рабочих: ", arra[c][5])
        print("Прибыль от работы экскаватора: ", arra[c][6])
        print("Прибыль от работы бульдозера: ", arra[c][7])
        print("  --------------------------------------")
        print("Итого прибыль: ", arra[c][7]+arra[c][6]-arra[c][5]-arra[c][4]-arra[c][3])
        print("\n")
        c = input("Выберите день для вывода(-1 - отмена, всего 1000): ")